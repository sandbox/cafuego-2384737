<?php
/**
 * @file
 */

/**
 * Implements hook_views_data().
 */
function views_search_api_db_suggestions_area_views_data() {
  $data['views']['views_search_api_db_suggestions_area'] = array(
    'title' => t('Search suggestions'),
    'group' => t('Search API'),
    'help' => t('Provide search suggestions for the view.'),
    'area' => array(
      'help' => t('Provide search suggestions for the view.'),
      'handler' => 'views_search_api_db_suggestions_area',
    ),
  );

  return $data;
}
